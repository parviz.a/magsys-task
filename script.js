
const FACT_API_URL = `https://catfact.ninja/fact`

const $ = function(selector){
    return document.querySelector(selector);
}

const displayLoading = (loaderContainer, textContainer) => {
    loaderContainer.style.display = 'flex';
    textContainer.style.display = 'none';
};

const hideLoading = (loaderContainer, textContainer) => {
    loaderContainer.style.display = 'none';
    textContainer.style.display = "flex";
};
async function fetchRandomCatFacts() {
    const response = await fetch(FACT_API_URL)
    const fact = await response.json();
    return fact;
}

function displayFact(content) {
    let loaderContainer = content.querySelector('.loader');
    let textContainer = content.querySelector('.accordionContent');

    let text = content.querySelector('.accordionContent p');

    displayLoading(loaderContainer, textContainer);

    fetchRandomCatFacts().then( fact => {
        text.innerHTML = fact.fact;
    }).catch((e) => {
        console.log(e)
    }).finally(() => {
        hideLoading(loaderContainer, textContainer);
    });
}

/* ==================================================
    ACCORDION
 ===================================================*/

const accordion = $('.accordion__container');

const anchors = document.querySelectorAll('.accordion__list a');
const lastAnchor = anchors[anchors.length - 1];
const logoImage = $('#logo');

lastAnchor.addEventListener('mouseenter', () => {
    logoImage.style.backgroundImage = "url('./images/Logo_inverted.svg')"
});
lastAnchor.addEventListener('mouseleave', () => {
    logoImage.style.backgroundImage = "url('./images/Logo.svg')"
});

accordion.addEventListener("click",function(e) {
    e.stopPropagation();
    e.preventDefault();
    if(e.target && e.target.nodeName == "A") {
        const classes = e.target.className.split(" ");
        if(classes) {
            for(let x = 0; x < classes.length; x++) {
                if(classes[x] == "accordionTitle") {
                    let title = e.target;
                    let parent = e.target.parentNode;
                    let content = e.target.parentNode.nextElementSibling;
                    classie.toggle(title, 'accordionTitleActive');
                    classie.toggle(parent, 'activeAccordion');
                    if(classie.has(content, 'accordionItemCollapsed')) {
                        displayFact(content);
                        if(classie.has(content, 'animateOut')){
                            classie.remove(content, 'animateOut');
                        }
                        classie.add(content, 'animateIn');
                    }else{
                        classie.remove(content, 'animateIn');
                        classie.add(content, 'animateOut');
                    }
                    classie.toggle(content, 'accordionItemCollapsed');
                }
            }
        }
    }
});